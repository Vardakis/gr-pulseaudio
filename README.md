## Pulseaudio GNURadio Sink/Source blocks

### Dependencies

The pulseaudio development files are needed for compilation of gr-pulseaudio. The package name is 'libpulse-dev' or 'libpulse-devel' depending on the distribution


### Installation

```bash
git clone https://gitlab.com/Vardakis/gr-pulseaudio
cd gr-pulseaudio
gr_modtool bind pa_sink
gr_modtool bind pa_source
mkdir build
cd build
cmake ..
make -j $(nproc --all)
sudo make install
```

If this is the first time you are building the gr-pulseaudio module run
`sudo ldconfig`

### Binding after header editing

Because of the newest GNURadio binding scheme, the python bindings need to be updated every time a header file of a block is altered. For this reason the command
`gr_modtool bind block_name` must be run inside the root directory of the OOT before cmake.

#### Advanced
By default, the **gr-pulseaudio** module will use the default installation prefix.
This highly depends on the Linux distribution. You can use the `CMAKE_INSTALL_PREFIX`
variable to alter the default installation path.
E.g:

`cmake -DCMAKE_INSTALL_PREFIX=/usr ..`

