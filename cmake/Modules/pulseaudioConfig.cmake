find_package(PkgConfig)

PKG_CHECK_MODULES(PC_PULSEAUDIO pulseaudio)

FIND_PATH(
    PULSEAUDIO_INCLUDE_DIRS
    NAMES pulseaudio/api.h
    HINTS $ENV{PULSEAUDIO_DIR}/include
        ${PC_PULSEAUDIO_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    PULSEAUDIO_LIBRARIES
    NAMES gnuradio-pulseaudio
    HINTS $ENV{PULSEAUDIO_DIR}/lib
        ${PC_PULSEAUDIO_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/pulseaudioTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(PULSEAUDIO DEFAULT_MSG PULSEAUDIO_LIBRARIES PULSEAUDIO_INCLUDE_DIRS)
MARK_AS_ADVANCED(PULSEAUDIO_LIBRARIES PULSEAUDIO_INCLUDE_DIRS)
