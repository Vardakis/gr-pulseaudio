/*
 * Copyright 2022 Free Software Foundation, Inc.
 *
 * This file is part of GNU Radio
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 */

/***********************************************************************************/
/* This file is automatically generated using bindtool and can be manually edited  */
/* The following lines can be configured to regenerate this file during cmake      */
/* If manual edits are made, the following tags should be modified accordingly.    */
/* BINDTOOL_GEN_AUTOMATIC(0)                                                       */
/* BINDTOOL_USE_PYGCCXML(0)                                                        */
/* BINDTOOL_HEADER_FILE(pa_source.h)                                        */
/* BINDTOOL_HEADER_FILE_HASH(4fa0ede5702e924c9f375ef2108110ca)                     */
/***********************************************************************************/

#include <pybind11/complex.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

namespace py = pybind11;

#include <pulseaudio/pa_source.h>
// pydoc.h is automatically generated in the build directory
#include <pa_source_pydoc.h>

void bind_pa_source(py::module& m)
{

    using pa_source    = gr::pulseaudio::pa_source;


    py::class_<pa_source, gr::hier_block2,
        std::shared_ptr<pa_source>>(m, "pa_source", D(pa_source))

        .def(py::init(&pa_source::make),
           D(pa_source,make)
        )
        



        ;




}








