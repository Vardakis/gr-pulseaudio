/* -*- c++ -*- */
/*
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <gnuradio/io_signature.h>
#include <boost/thread/thread.hpp>
#include "pa_sink_impl.h"
#include <chrono>

namespace gr {
  namespace pulseaudio {

    using input_type = float;

    pa_sink::sptr
    pa_sink::make(const std::string device_name, int audio_rate, const std::string app_name, const std::string stream_name)
    {
      return gnuradio::make_block_sptr<pa_sink_impl>(
        device_name, audio_rate, app_name, stream_name);
    }


    /*
     * The private constructor
     */
    pa_sink_impl::pa_sink_impl(const std::string device_name, int audio_rate, const std::string app_name, const std::string stream_name)
      : gr::sync_block("pa_sink",
              gr::io_signature::make(1 , 2, sizeof(input_type)),
              gr::io_signature::make(0,0,0)),
			    d_stream_name(stream_name),
			    d_app_name(app_name),
				d_msg_out(pmt::mp("msg_out")),
				d_threshold(200),
				d_tx_status(true)
    {
        int error;

        /* The sample type to use */
        d_ss.format = PA_SAMPLE_FLOAT32LE;
        d_ss.rate = audio_rate;
        d_ss.channels = 2;
        d_pasink = pa_simple_new(NULL,
                                 d_app_name.c_str(),
                                 PA_STREAM_PLAYBACK,
                                 device_name.empty() ? NULL : device_name.c_str(),
                                 d_stream_name.c_str(),
                                 &d_ss,
                                 NULL,
                                 NULL,
                                 &error);

        if (!d_pasink) {
            throw std::runtime_error("pa_sink: PA simple new failed");
        }
        message_port_register_out(d_msg_out);
        d_timer_thread =
            new boost::thread(boost::bind(&pa_sink_impl::timer_thread, this));
        d_stop_thread = false;
    }

    /*
     * Our virtual destructor.
     */
    pa_sink_impl::~pa_sink_impl()
    {
        if (d_pasink) {
            pa_simple_free(d_pasink);
        }
    }

    bool pa_sink_impl::start()
    {
        return true;
    }

    bool pa_sink_impl::stop()
    {
        return true;
    }

    void pa_sink_impl::timer_thread(){
    	boost::this_thread::sleep_for(boost::chrono::seconds(3));
    	while(!d_stop_thread){
    		auto stop = std::chrono::high_resolution_clock::now();
    		auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>(stop - d_start);
    		if(int_ms.count() > d_threshold){
    			if(d_tx_status){
    				d_tx_status = false;
    				message_port_pub(d_msg_out, pmt::from_bool(false));
    			}
    		}
    		boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
    	}
    }

    int
    pa_sink_impl::work(int noutput_items,
        gr_vector_const_void_star &input_items,
        gr_vector_void_star &output_items)
    {

    	d_start = std::chrono::high_resolution_clock::now();
    	if(!d_tx_status){
    		message_port_pub(d_msg_out, pmt::from_bool(true));
    		d_tx_status = true;
    	}
        float *ptr = &d_audio_buffer[0];
        int i, error;

        (void) output_items;

        if (noutput_items > BUFFER_SIZE/2)
            noutput_items = BUFFER_SIZE/2;

        if (input_items.size() == 2)
        {
            // two channels (stereo)
            const float *data_l = (const float*) input_items[0]; // left channel
            const float *data_r = (const float*) input_items[1]; // right channel
            for (i = noutput_items; i > 0; i--)
            {
                *ptr++ = *data_l++;
                *ptr++ = *data_r++;
            }
        }
        else
        {
            // assume 1 channel (mono)
            const float *data = (const float*) input_items[0];
            for (i = noutput_items; i > 0; i--)
            {
                float a = *data++;
                *ptr++ = a; // same data in left and right channel
                *ptr++ = a;
            }
        }
        if (pa_simple_write(d_pasink, d_audio_buffer, 2*noutput_items*sizeof(float), &error) < 0) { //!!!
        	throw std::runtime_error("pa_sink: PA simple write failed");
        }
        d_start = std::chrono::high_resolution_clock::now();
        return noutput_items;
    }

  } /* namespace pulseaudio */
} /* namespace gr */
