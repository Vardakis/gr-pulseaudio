/* -*- c++ -*- */
/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PULSEAUDIO_PA_SINK_IMPL_H
#define INCLUDED_PULSEAUDIO_PA_SINK_IMPL_H

#include <pulseaudio/pa_sink.h>
#include <pulse/simple.h>

#define BUFFER_SIZE 100000

namespace gr {
  namespace pulseaudio {

    class pa_sink_impl : public pa_sink
    {
     private:
        pa_simple*					d_pasink;    /*! The pulseaudio object. */
        std::string 				d_stream_name;   /*! Descriptive name of the stream. */
        std::string 				d_app_name;      /*! Descriptive name of the application. */
        pa_sample_spec 				d_ss;    /*! pulseaudio sample specification. */
        float 						d_audio_buffer[BUFFER_SIZE];
        pmt::pmt_t 					d_msg_out;
        boost::thread* 				d_timer_thread;
        std::chrono::high_resolution_clock::time_point
									d_start;
        size_t 						d_threshold;
        bool						d_tx_status;
        bool                        d_stop_thread;

        void timer_thread();


     public:
      pa_sink_impl(const std::string device_name, int audio_rate, const std::string app_name, const std::string stream_name);
      ~pa_sink_impl();

      bool start();
      bool stop();

      // Where all the action really happens
      int work(
              int noutput_items,
              gr_vector_const_void_star &input_items,
              gr_vector_void_star &output_items
      );
    };

  } // namespace pulseaudio
} // namespace gr

#endif /* INCLUDED_PULSEAUDIO_PA_SINK_IMPL_H */
