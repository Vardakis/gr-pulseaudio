# Copyright 2011,2012,2016,2018,2019 Free Software Foundation, Inc.
#
# This file was generated by gr_modtool, a tool from the GNU Radio framework
# This file is a part of gr-pulseaudio
#
# SPDX-License-Identifier: GPL-3.0-or-later
#

########################################################################
# Setup library
########################################################################
include(GrPlatform) #define LIB_SUFFIX

list(APPEND pulseaudio_sources
    pa_sink_impl.cc
    sound_detector_impl.cc
    pa_sample_source_impl.cc
    pa_source_impl.cc
)

find_path(PULSEAUDIO_INCLUDE_DIR
        NAMES pulse/pulseaudio.h
        DOC "The PulseAudio include directory"
        )

find_library(PULSEAUDIO_LIBRARY
        NAMES pulse
        DOC "The PulseAudio library"
        )

set(pulseaudio_sources "${pulseaudio_sources}" PARENT_SCOPE)
if(NOT pulseaudio_sources)
    MESSAGE(STATUS "No C++ sources... skipping lib/")
    return()
endif(NOT pulseaudio_sources)

add_library(gnuradio-pulseaudio SHARED ${pulseaudio_sources})
find_package(Boost COMPONENTS thread chrono REQUIRED )

INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIRS})

target_link_libraries(gnuradio-pulseaudio
					 pulse-simple
					 pulse
					 ${Boost_LIBRARIES}
					 gnuradio::gnuradio-runtime)
target_include_directories(gnuradio-pulseaudio
    PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/../include>
    PUBLIC $<INSTALL_INTERFACE:include>
  )
set_target_properties(gnuradio-pulseaudio PROPERTIES DEFINE_SYMBOL "gnuradio_pulseaudio_EXPORTS")

if(APPLE)
    set_target_properties(gnuradio-pulseaudio PROPERTIES
        INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/lib"
    )
endif(APPLE)

########################################################################
# Install built library files
########################################################################
include(GrMiscUtils)
GR_LIBRARY_FOO(gnuradio-pulseaudio)

########################################################################
# Print summary
########################################################################
message(STATUS "Using install prefix: ${CMAKE_INSTALL_PREFIX}")
message(STATUS "Building for version: ${VERSION} / ${LIBVER}")

########################################################################
# Build and register unit test
########################################################################
include(GrTest)

# If your unit tests require special include paths, add them here
#include_directories()
# List all files that contain Boost.UTF unit tests here
list(APPEND test_pulseaudio_sources
qa_pa_sink.cc
)
# Anything we need to link to for the unit tests go here
list(APPEND GR_TEST_TARGET_DEPS gnuradio-pulseaudio)

if(NOT test_pulseaudio_sources)
    MESSAGE(STATUS "No C++ unit tests... skipping")
    return()
endif(NOT test_pulseaudio_sources)

