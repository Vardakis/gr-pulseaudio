/* -*- c++ -*- */
/*
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#ifndef INCLUDED_PULSEAUDIO_PA_SINK_H
#define INCLUDED_PULSEAUDIO_PA_SINK_H

#include <pulseaudio/api.h>
#include <gnuradio/sync_block.h>

namespace gr {
  namespace pulseaudio {

    /*!
     * \brief Pulseaudio sink block
     * \ingroup pulseaudio
     *
     */
    class PULSEAUDIO_API pa_sink : virtual public gr::sync_block
    {
     public:
      typedef std::shared_ptr<pa_sink> sptr;

      /*!
       *  \param device_name The name of the audio device, or NULL for default.
       *  \param audio_rate The sample rate of the audio stream.
       *  \param app_name Application name.
       *  \param stream_name The audio stream name.
       */

      static sptr make(const std::string device_name, int audio_rate, const std::string app_name, const std::string stream_name);
    };

  } // namespace pulseaudio
} // namespace gr

#endif /* INCLUDED_PULSEAUDIO_PA_SINK_H */
